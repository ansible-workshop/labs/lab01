# Ansible - Workshop
Lab 01: Install Ansible and configure managed node

---

# Instructions

 - Install ansible on ubuntu 18.04
 - Create ssh key and copy to remote machine
 - Configure remote machine to enable ansible to run it.

---

## Install ansible on ubuntu 18.04
```
$ ssh (username)@(your control machine ip) - For example - ssh sela@34.76.50.179
$ sudo apt install ansible
$ sudo apt upgrade -y
When promped for services restart authorization choose yes
```


## Create ssh key and copy to remote machine
# Do it from the Master node to each node (Slave)
```
$ ssh-keygen
press enter for filename
Overwrite - Y
press enter twice with passphrase empty
$ ssh-copy-id -i ~/.ssh/id_rsa.pub (username)@(node machine ip) - For example - ssh-copy-id -i sela@34.76.142.118
When promped whether to continue connecting press yes
```


## Configure remote machines (Slaves) to enable ansible to run commands on it.
```
$ ssh (username)@(node machine ip)
$ sudo apt upgrade -y
$ sudo apt install python
```

## Change the file value for pubkeyAuthentication to "yes"
```
$ sudo vim /etc/ssh/sshd_config
press i
```

## Search for the pubkeyAuthentication attribute and change its value from no to yes - If it is already set to "yes" you can exit the editor with pressing esc and then press :q
```
#LoginGraceTime 2m
#PermitRootLogin prohibit-password
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

PubkeyAuthentication no

# Expect .ssh/authorized_keys2 to be disregarded by default in future.
#AuthorizedKeysFile     .ssh/authorized_keys .ssh/authorized_keys2

#AuthorizedPrincipalsFile none'
```

## Save changes
```
Press esc
press :wq
```

## Restart the ssh service and exit to control machine - if the value is correct already - skip.
```
$ sudo service ssh restart
$ sudo systemctl reload sshd

```

## Exit node machine
```
$ exit

```

# Do the steps again for each slave node!


## Edit the hosts file (On Master)

### Open hosts file in editor
```
$ sudo vim /etc/ansible/hosts

The existing ip is a place holder - you need to replace it.

press i
```
### Add the following entry - instead of (nodeip) enter the nodes ips (without brackets) you recieved - for example - 146.148.127.157
```
(nodeips)
```

### Save entry and exit to terminal
```
press esc
press :wq
```

## Ensure valid connection between control machine and managed node machine
```
$ ansible -m ping all
``` 

The result should be as : 

(node machine ip) | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
